---
title: Ubuntu主题美化
date: 2017-03-16 22:04:16
tags:
 - ubuntu
---
> 最近发现一款Ubuntu的主题很好看，分享一下

<!--more-->
# Flatabulous
![img](3.png)
### 1.安装主题的第一步是安装Ubuntu tweak tool，安装命令如下：
```bash	
	sudo add-apt-repository ppa:tualatrix/ppa 
	sudo apt-get update 
	sudo apt-get install ubuntu-tweak 
```
### 2.下载主题包 https://github.com/anmoljagetia/Flatabulous/archive/master.zip
### 3.在/usr/share/themes/文件夹中放置解压后文件，在此之前应先赋予文件夹权限
```bash	
	sudo chmod 777 /usr/share/themes/
```
![img](1.png)
### 4.安装图标 
```bash
	sudo add-apt-repository ppa:noobslab/icons
	sudo apt-get update
	sudo apt-get install ultra-flat-icons
	sudo apt-get install ultra-flat-icons-orange
	sudo apt-get install ultra-flat-icons-green
```
![img](2.png)