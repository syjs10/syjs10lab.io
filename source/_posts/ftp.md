---
title: ftp搭建与使用
date: 2016-10-15 16:58:48
tags:
 - ubuntu
---
> ftp文件传输
<!--more-->


## 本机安装ftp
      sudo apt-get install ftp
## 远端服务器开启ftp服务
### 1.安装vsftpd

      sudo apt-get install vsftpd

安装完毕后或许会自动生成一个帐户"ftp"，/home下也会增加一个文件夹。
如果没有生成这个用户的话可以手动来，生成了就不用了：

      sudo useradd -m ftp
      sudo passwd ftp

有"ftp"帐户后还要更改权限

      sudo chmod 777 /home/ftp


### 2.上传文件可能出现问题：550 Permission denied
原因：vsftp默认配置不允许上传文件。
解决：修改/etc/vsftpd.conf
将“write_enable=YES”前面的#取消。
重启ftp服务

      sudo /etc/init.d/vsftpd restart
      sudo /etc/init.d/vsftpd start
      sudo /etc/init.d/vsftpd stop

## 文件传输命令
### 1.连接ftp服务器
ftp [hostname| ip-address]

      $ ftp xxx.xxx.xxx.xxx

### 2.下载文件
下载文件通常用get和mget这两条命令。

a) get
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格式：get [remote-file] [local-file]
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;将文件从远端主机中传送至本地主机中.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如要获取服务器上 /home/ftp/1.bmp,则

      ftp>  get /home/ftp/1.bmp 1.bmp

b) mget　　　　　　
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格式：mget [remote-files]
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;从远端主机接收一批文件至本地主机.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如要获取服务器上/home/ftp下的所有文件,则

      ftp>  cd  /home/ftp
      ftp>  mget  *.*

### 3.上传文件
a) put
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格式：put local-file [remote-file]
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;将本地一个文件传送至远端主机中.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如要把本地的1.bmp传送到远端主机/home/ftp,并改名为333.bmp

      ftp>  put 1.bmp /home/ftp/333.bmp

b) mput
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格式：mput local-files
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;将本地主机中一批文件传送至远端主机.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如要把本地当前目录下所有bmp文件上传到服务器/home/ftp 下

      ftp>  cd  /home/ftp
      ftp>  mput  *.bmp

###4. 断开连接

      ftp>  bye
