---
title: http服务器源码阅读
date: 2022-04-11 00:07:06
tags: C HTTP 服务器
---

> 找到一个小型的http服务器的源码，阅读学习一下，源码地址 https://github.com/EZLippi/Tinyhttpd

<!-- more -->

# main函数
首先是main函数，这里是程序的主流程，接下来以注释的形式进行阅读
```c
int main(void)
{
    int server_sock = -1;
    u_short port = 4000;
    int client_sock = -1;
    struct sockaddr_in client_name;
    socklen_t  client_name_len = sizeof(client_name);
    pthread_t newthread;
    // 这里进行了sock的初始化
    server_sock = startup(&port);
    printf("httpd running on port %d\n", port);

    while (1)
    {
        // 监听server_sock的请求，有请求的时候生成一个client_sock
        client_sock = accept(server_sock,
                (struct sockaddr *)&client_name,
                &client_name_len);
        if (client_sock == -1)
            error_die("accept");
        /* accept_request(&client_sock); */
        // 创建一个线程用于处理本次请求
        if (pthread_create(&newthread , NULL, (void *)accept_request, (void *)(intptr_t)client_sock) != 0)
            perror("pthread_create");
    }

    close(server_sock);

    return(0);
}
```
## socket 关键函数

### accept 函数

```c
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>

int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
```

#### 参数说明

sockdf: socket文件描述符
addr: 传出参数，返回链接客户端地址信息，含IP地址和端口号
addrlen: 传入传出参数（值-结果）,传入sizeof(addr)大小，函数返回时返回真正接收到地址结构体的大小

#### 函数返回值
成功返回一个新的socket文件描述符，用于和客户端通信，失败返回-1，设置errno

#### 函数说明

三方握手完成后，服务器调用accept()接受连接，如果服务器调用accept()时还没有客户端的连接请求，就阻塞等待直到有客户端连接上来。
addr是一个传出参数，accept()返回时传出客户端的地址和端口号。
addrlen参数是一个传入传出参数（value-result argument），传入的是调用者提供的缓冲区addr的长度以避免缓冲区溢出问题，传出的是客户端地址结构体的实际长度（有可能没有占满调用者提供的缓冲区）。
如果给addr参数传NULL，表示不关心客户端的地址。

#### 程序示例

```c
while (1)
{
    cliaddr_len = sizeof(cliaddr);
    connfd = accept(listenfd, (struct sockaddr *)&cliaddr, &cliaddr_len);
    n = read(connfd, buf, MAXLINE);
    ......
    close(connfd);
}
```
整个是一个while死循环，每次循环处理一个客户端连接。
由于cliaddr_len是传入传出
参数，每次调用accept()之前应该重新赋初值。
accept()的参数listenfd是先前的监听文件描述符，而accept()的返回值是另外一个文件描述符connfd，之后与客户端之间就通过这个connfd通讯，最后关闭connfd断开连接，而不关闭listenfd，再次回到循环开头listenfd仍然用作accept的参数。
accept()成功返回一个文件描述符，出错返回-1。

## 线程函数

### pthread_create

```c
#include <pthread.h>

int pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg);
```

#### 参数说明

thread：传递一个 pthread_t 类型的指针变量，也可以直接传递某个 pthread_t 类型变量的地址。pthread_t 是一种用于表示线程的数据类型，每一个 pthread_t 类型的变量都可以表示一个线程。

attr：用于手动设置新建线程的属性，例如线程的调用策略、线程所能使用的栈内存的大小等。大部分场景中，我们都不需要手动修改线程的属性，将 attr 参数赋值为 NULL，pthread_create() 函数会采用系统默认的属性值创建线程。

start_routine: 以函数指针的方式指明新建线程需要执行的函数，该函数的参数最多有 1 个（可以省略不写），形参和返回值的类型都必须为 void* 类型。void* 类型又称空指针类型，表明指针所指数据的类型是未知的。使用此类型指针时，我们通常需要先对其进行强制类型转换，然后才能正常访问指针指向的数据。

arg：指定传递给 start_routine 函数的实参，当不需要传递任何数据时，将 arg 赋值为 NULL 即可。



# startup函数
```c
/**********************************************************************/
/* This function starts the process of listening for web connections
 * on a specified port.  If the port is 0, then dynamically allocate a
 * port and modify the original port variable to reflect the actual
 * port.
 * Parameters: pointer to variable containing the port to connect on
 * Returns: the socket */
/**********************************************************************/
int startup(u_short *port)
{
    int httpd = 0;
    int on = 1;
    struct sockaddr_in name;

    // 申请socket句柄，IPv4的tcp链接， 面向流的传输协议
    httpd = socket(PF_INET, SOCK_STREAM, 0);
    if (httpd == -1)
        error_die("socket");
    memset(&name, 0, sizeof(name));
    // 设置IPv4的地址
    name.sin_family = AF_INET;
    name.sin_port = htons(*port);
    name.sin_addr.s_addr = htonl(INADDR_ANY);
    // 开启地址复用功能
    if ((setsockopt(httpd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on))) < 0)
    {
        error_die("setsockopt failed");
    }
    // 将socket句柄绑定在ipport上
    if (bind(httpd, (struct sockaddr *)&name, sizeof(name)) < 0)
        error_die("bind");

    // 如果传入的端口为0，则使用sockaddr_in自动生成的端口，此处就是在获取端口
    if (*port == 0)  /* if dynamically allocating a port */
    {
        socklen_t namelen = sizeof(name);
        if (getsockname(httpd, (struct sockaddr *)&name, &namelen) == -1)
            error_die("getsockname");
        *port = ntohs(name.sin_port);
    }
    if (listen(httpd, 5) < 0)
        error_die("listen");
    return(httpd);
}
```

## sockaddr_in 数据结构
```c
// sin_port和sin_addr都必须是NBO
// 一般可视化的数字都是HBO（本机字节顺序）
struct sockaddr_in {
　　 short int sin_family;
    unsigned char sin_zero[8];
    unsigned short int sin_port;
    struct in_addr sin_addr;
}
struct in_addr {
    unsigned long s_addr;
}
// sin_zero 初始值应该使用函数 bzero() 来全部置零。
// 但是源码中貌似并没有使用这个函数清;
struct sockaddr_in cliaddr;
bzero(&cliaddr,sizeof(cliaddr));

// addr三种赋值方法
// 方法1
inet_aton(server_addr_string,&myaddr.sin_addr);
// 方法2
myaddr.sin_addr.s_addr = inet_addr("132.241.5.10");
// 方法3
//INADDR_ANY转不转NBO随便，因为都是0，INADDR_ANY是0.0.0.0
myaddr.sin_addr.s_addr = htons(INADDR_ANY);
myaddr.sin_addr.s_addr = INADDR_ANY;

// port 赋值方法
// 自定义端口
#define MYPORT 8000
myaddr.sin_port = htons(MYPORT);
// 随机端口
// 0（随机端口）转不转NBO随便
myaddr.sin_port = htons(0);
myaddr.sin_port = 0;

```

### AF_INET和PF_INET

1. AF 表示ADDRESS FAMILY 地址族
2. PF 表示PROTOCOL FAMILY 协议族

AF_INET 是IPv4的地址族
PF_INET 是IPv4的协议族

### 字节序的知识

1. 网络字节顺序 (Network Byte Order)      NBO
结构体的sin_port和sin_addr都必须是NBO
2. 本机字节顺序 (Host Byte Order)    HBO
一般可视化的数字都是HBO

NBO，HBO二者转换
inet_addr()    将字符串点数格式地址转化成无符号长整型（unsigned long s_addr s_addr;）
inet_aton()    将字符串点数格式地址转化成NBO
inet_ntoa ()     将NBO地址转化成字符串点数格式
htons()    "Host to Network Short"
htonl()    "Host to Network Long"
ntohs()    "Network to Host Short"
ntohl()    "Network to Host Long"

## sockaddr和sockaddr_in

程序员不应操作sockaddr，sockaddr是给操作系统用的
程序员应使用sockaddr_in来表示地址，sockaddr_in区分了地址和端口，使用更方便。
程序员把类型、ip地址、端口填充sockaddr_in结构体，然后强制转换成sockaddr，作为参数传递给系统调用函数

**{% asset_img 20170527135323019.jpeg  %}**
## socket 关键函数

### socket函数

socket()打开一个网络通讯端口，如果成功的话，就像open()一样返回一个文件描述符，应用程序可以像读写文件一样用read/write在网络上收发数据，如果socket()调用出错则返回-1。


```c
#include <sys/types.h>
#include <sys/socket.h>

int socket(int domain, int type, int protocol);
```

#### 参数说明

domain:
**AF_INET** 这是大多数用来产生socket的协议，使用TCP或UDP来传输，用IPv4的地址
**AF_INET6** 与上面类似，不过是来用IPv6的地址
**AF_UNIX** 本地协议，使用在Unix和Linux系统上，一般都是当客户端和服务器在同一台及其上的时候使用

type:
**SOCK_STREAM** 这个协议是按照顺序的、可靠的、数据完整的基于字节流的连接。这是一个使用最多的socket类型，这个socket是使用TCP来进行传输。
**SOCK_DGRAM** 这个协议是无连接的、固定长度的传输调用。该协议是不可靠的，使用UDP来进行它的连接。
**SOCK_SEQPACKET** 这个协议是双线路的、可靠的连接，发送固定长度的数据包进行传输。必须把这个包完整的接受才能进行读取。
**SOCK_RAW** 这个socket类型提供单一的网络访问，这个socket类型使用ICMP公共协议。（ping、traceroute使用该协议）
**SOCK_RDM** 这个类型是很少使用的，在大部分的操作系统上没有实现，它是提供给数据链路层使用，不保证数据包的顺序

protocol:
0 默认协议

#### 函数返回值

成功返回一个新的文件描述符，失败返回-1，设置errno

#### 函数说明

对于IPv4，domain参数指定为AF_INET。
对于TCP协议，type参数指定为SOCK_STREAM，表示面向流的传输协议。
如果是UDP协议，则type参数指定为SOCK_DGRAM，表示面向数据报的传输协议。
protocol参数的介绍略，指定为0即可。

### bind函数

bind()的作用是将参数sockfd和addr绑定在一起，使sockfd这个用于网络通讯的文件描述符监听addr所描述的地址和端口号。

```c
#include <sys/types.h>
#include <sys/socket.h>

int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
```

#### 参数说明

sockfd: socket文件描述符
addr: 构造出IP地址加端口号
addrlen: sizeof(addr)长度

#### 函数返回值

成功返回0，失败返回-1, 设置errno

#### 函数说明

服务器程序所监听的网络地址和端口号通常是固定不变的，客户端程序得知服务器程序
的地址和端口号后就可以向服务器发起连接，因此服务器需要调用bind绑定一个固定的网络地址和端口号。

### listen函数

```c
#include <sys/types.h>
#include <sys/socket.h>

int listen(int sockfd, int backlog);
```

#### 参数说明

sockfd: socket文件描述符
backlog: 排队建立3次握手队列和刚刚建立3次握手队列的链接数和

查看Linux系统默认backlog

    cat /proc/sys/net/ipv4/tcp_max_syn_backlog

#### 函数返回值
listen()成功返回0，失败返回-1。

#### 函数说明
典型的服务器程序可以同时服务于多个客户端，当有客户端发起连接时，服务器调用的accept()返回并接受这个连接，如果有大量的客户端发起连接而服务器来不及处理，尚未accept的客户端就处于连接等待状态，listen()声明sockfd处于监听状态，并且最多允许有backlog个客户端处于连接待状态，如果接收到更多的连接请求就忽略。

## setsockopt

```c
#include <sys/socket.h>

int setsockopt( int socket, int level, int option_name,const void *option_value, size_t ，ption_len);
```
第一个参数socket是套接字描述符。第二个参数level是被设置的选项的级别，如果想要在套接字级别上设置选项，就必须把level设置为 SOL_SOCKET。 option_name指定准备设置的选项，option_name可以有哪些取值，这取决于level，以linux 2.6内核为例

SO_REUSEADDR，打开或关闭地址复用功能。
当option_value不等于0时，打开，否则，关闭。它实际所做的工作是置sock->sk->sk_reuse为1或0。

# accept_request

```c
/**********************************************************************/
/* A request has caused a call to accept() on the server port to
 * return.  Process the request appropriately.
 * Parameters: the socket connected to the client */
/**********************************************************************/
void accept_request(void *arg)
{
    int client = (intptr_t)arg;
    char buf[1024];
    size_t numchars;
    char method[255];
    char url[255];
    char path[512];
    size_t i, j;
    struct stat st;
    int cgi = 0;      /* becomes true if server decides this is a CGI
                       * program */
    char *query_string = NULL;
    // 从句柄中读取请求头信息
    numchars = get_line(client, buf, sizeof(buf));
    i = 0;
    j = 0;
    // 获取请求method信息
    while (!ISspace(buf[i]) && (i < sizeof(method) - 1))
    {
        method[i] = buf[i];
        i++;
    }
    j=i;
    method[i] = '\0';
    // 这个小型的服务器只接收GET和POST请求
    if (strcasecmp(method, "GET") && strcasecmp(method, "POST"))
    {
        // 返回5xx页面
        unimplemented(client);
        return;
    }
    // POST请求
    if (strcasecmp(method, "POST") == 0)
        cgi = 1;
    // 读取多余空格
    i = 0;
    while (ISspace(buf[j]) && (j < numchars))
        j++;
    // 获得uri字符串
    while (!ISspace(buf[j]) && (i < sizeof(url) - 1) && (j < numchars))
    {
        url[i] = buf[j];
        i++; j++;
    }
    url[i] = '\0';
    // GET 请求
    if (strcasecmp(method, "GET") == 0)
    {
        query_string = url;
        while ((*query_string != '?') && (*query_string != '\0'))
            query_string++;
        // 获取请求参数字符串
        if (*query_string == '?')
        {
            cgi = 1;
            *query_string = '\0';
            query_string++;
        }
    }
    // 这里拼接文件路径，html默认路径为htdocs
    sprintf(path, "htdocs%s", url);
    if (path[strlen(path) - 1] == '/')
        strcat(path, "index.html");
    // 如果文件不存在，则丢弃无用的header信息返回404错误页面
    if (stat(path, &st) == -1) {
        while ((numchars > 0) && strcmp("\n", buf))  /* read & discard headers */
            numchars = get_line(client, buf, sizeof(buf));
        not_found(client);
    }
    else
    {
        // 如果path解析的文件类型是一个目录，则在这个路径下查找html文件
        if ((st.st_mode & S_IFMT) == S_IFDIR)
            strcat(path, "/index.html");
        // 是否有该文件的执行权限
        if ((st.st_mode & S_IXUSR) ||
                (st.st_mode & S_IXGRP) ||
                (st.st_mode & S_IXOTH)    )
            cgi = 1;
        // cgi 是0就说明直接读取文件返回，1就是需要执行cgi程序
        if (!cgi)
            serve_file(client, path);
        else
            execute_cgi(client, path, method, query_string);
    }
    // 关闭句柄
    close(client);
}

int get_line(int sock, char *buf, int size)
{
    int i = 0;
    char c = '\0';
    int n;

    while ((i < size - 1) && (c != '\n'))
    {
        n = recv(sock, &c, 1, 0);
        /* DEBUG printf("%02X\n", c); */
        if (n > 0)
        {
            if (c == '\r')
            {
                n = recv(sock, &c, 1, MSG_PEEK);
                /* DEBUG printf("%02X\n", c); */
                if ((n > 0) && (c == '\n'))
                    recv(sock, &c, 1, 0);
                else
                    c = '\n';
            }
            buf[i] = c;
            i++;
        }
        else
            c = '\n';
    }
    buf[i] = '\0';

    return(i);
}
```

## socket 函数

### recv 函数

```c
#include <sys/socket.h>

int recv( SOCKET s, char FAR *buf, int len, int flags);
```

#### 函数参数

s: 不论是客户还是服务器应用程序都用recv函数从TCP连接的另一端接收数据。该函数的第一个参数指定接收端套接字描述符
buf: 数指明一个缓冲区，该缓冲区用来存放recv函数接收到的数据
len: 参数指明buf的长度
flags: 一般设置为0，此时send为阻塞式发送

#### 函数返回值

默认 socket 是阻塞的 解阻塞与非阻塞recv返回值没有区分，都是 <0 出错 ；=0 连接关闭 ；>0 接收到数据大小，
返回值<0时并且(errno == EINTR || errno == EWOULDBLOCK || errno == EAGAIN)的情况下认为连接是正常的，继续接收。

##### errno被设为以下的某个值
EAGAIN: 套接字已标记为非阻塞，而接收操作被阻塞或者接收超时
EBADF: sock不是有效的描述词
ECONNREFUSE: 远程主机阻绝网络连接
EFAULT: 内存空间访问出错
EINTR: 操作被信号中断
EINVAL: 参数无效
ENOMEM: 内存不足
ENOTCONN: 与面向连接关联的套接字尚未被连接上
ENOTSOCK: sock索引的不是套接字 当返回值是0时，为正常关闭连接；

## 文件操作函数

### stat 函数

```c
#include <sys/stat.h>
#include <unistd.h>

int stat(const char * file_name, struct stat *buf);

struct stat
{
    dev_t st_dev; //device 文件的设备编号
    ino_t st_ino; //inode 文件的i-node
    mode_t st_mode; //protection 文件的类型和存取的权限
    nlink_t st_nlink; //number of hard links 连到该文件的硬连接数目, 刚建立的文件值为1.
    uid_t st_uid; //user ID of owner 文件所有者的用户识别码
    gid_t st_gid; //group ID of owner 文件所有者的组识别码
    dev_t st_rdev; //device type 若此文件为装置设备文件, 则为其设备编号
    off_t st_size; //total size, in bytes 文件大小, 以字节计算
    unsigned long st_blksize; //blocksize for filesystem I/O 文件系统的I/O 缓冲区大小.
    unsigned long st_blocks; //number of blocks allocated 占用文件区块的个数, 每一区块大小为512 个字节.
    time_t st_atime; //time of lastaccess 文件最近一次被存取或被执行的时间, 一般只有在用mknod、utime、read、write 与tructate 时改变.
    time_t st_mtime; //time of last modification 文件最后一次被修改的时间, 一般只有在用mknod、utime 和write 时才会改变
    time_t st_ctime; //time of last change i-node 最近一次被更改的时间, 此参数会在文件所有者、组、权限被更改时更新
};
```

#### 函数说明
stat()用来将参数file_name 所指的文件状态, 复制到参数buf 所指的结构中。

#### 函数参数
file_name: 文件名
buf: 文件信息结构体

st_mode 则定义了下列数种情况：
1、S_IFMT 0170000 文件类型的位遮罩
2、S_IFSOCK 0140000 scoket
3、S_IFLNK 0120000 符号连接
4、S_IFREG 0100000 一般文件
5、S_IFBLK 0060000 区块装置
6、S_IFDIR 0040000 目录
7、S_IFCHR 0020000 字符装置
8、S_IFIFO 0010000 先进先出
9、S_ISUID 04000 文件的 (set user-id on execution)位
10、S_ISGID 02000 文件的 (set group-id on execution)位
11、S_ISVTX 01000 文件的sticky 位
12、S_IRUSR (S_IREAD) 00400 文件所有者具可读取权限
13、S_IWUSR (S_IWRITE)00200 文件所有者具可写入权限
14、S_IXUSR (S_IEXEC) 00100 文件所有者具可执行权限
15、S_IRGRP 00040 用户组具可读取权限
16、S_IWGRP 00020 用户组具可写入权限
17、S_IXGRP 00010 用户组具可执行权限
18、S_IROTH 00004 其他用户具可读取权限
19、S_IWOTH 00002 其他用户具可写入权限
20、S_IXOTH 00001 其他用户具可执行权限上述的文件类型在 POSIX 中定义了检查这些类型的宏定义
21、S_ISLNK (st_mode) 判断是否为符号连接
22、S_ISREG (st_mode) 是否为一般文件
23、S_ISDIR (st_mode) 是否为目录
24、S_ISCHR (st_mode) 是否为字符装置文件
25、S_ISBLK (s3e) 是否为先进先出
26、S_ISSOCK (st_mode) 是否为socket 若一目录具有sticky 位 (S_ISVTX), 则表示在此目录下的文件只能被该文件所有者、此目录所有者或root 来删除或改名.

#### 返回值

执行成功则返回0，失败返回-1，错误代码存于errno。

错误代码：
1、ENOENT 参数file_name 指定的文件不存在
2、ENOTDIR 路径中的目录存在但却非真正的目录
3、ELOOP 欲打开的文件有过多符号连接问题, 上限为16 符号连接
4、EFAULT 参数buf 为无效指针, 指向无法存在的内存空间
5、EACCESS 存取文件时被拒绝
6、ENOMEM 核心内存不足
7、ENAMETOOLONG 参数file_name 的路径名称太长

# serve_file

将文件原样返回给web页面，只能处理请求html的get请求

```c
/**********************************************************************/
/* Send a regular file to the client.  Use headers, and report
 * errors to client if they occur.
 * Parameters: a pointer to a file structure produced from the socket
 *              file descriptor
 *             the name of the file to serve */
/**********************************************************************/
void serve_file(int client, const char *filename)
{
    FILE *resource = NULL;
    int numchars = 1;
    char buf[1024];
    // 不太理解为什么要赋值一个A，怀疑是初始化这个buf
    buf[0] = 'A'; buf[1] = '\0';
    while ((numchars > 0) && strcmp("\n", buf))  /* read & discard headers */
        numchars = get_line(client, buf, sizeof(buf));

    resource = fopen(filename, "r");
    if (resource == NULL)
        not_found(client);
    else
    {
        // 输出200 的header
        headers(client, filename);
        // 将文件内容原样输出到屏幕
        cat(client, resource);
    }
    fclose(resource);
}
/**********************************************************************/
/* Put the entire contents of a file out on a socket.  This function
 * is named after the UNIX "cat" command, because it might have been
 * easier just to do something like pipe, fork, and exec("cat").
 * Parameters: the client socket descriptor
 *             FILE pointer for the file to cat */
/**********************************************************************/
void cat(int client, FILE *resource)
{
    char buf[1024];

    fgets(buf, sizeof(buf), resource);
    while (!feof(resource))
    {
        send(client, buf, strlen(buf), 0);
        fgets(buf, sizeof(buf), resource);
    }
}
/**********************************************************************/
/* Return the informational HTTP headers about a file. */
/* Parameters: the socket to print the headers on
 *             the name of the file */
/**********************************************************************/
void headers(int client, const char *filename)
{
    char buf[1024];
    (void)filename;  /* could use filename to determine file type */

    strcpy(buf, "HTTP/1.0 200 OK\r\n");
    send(client, buf, strlen(buf), 0);
    strcpy(buf, SERVER_STRING);
    send(client, buf, strlen(buf), 0);
    sprintf(buf, "Content-Type: text/html\r\n");
    send(client, buf, strlen(buf), 0);
    strcpy(buf, "\r\n");
    send(client, buf, strlen(buf), 0);
}
```

# execute_cgi

```c
/**********************************************************************/
/* Execute a CGI script.  Will need to set environment variables as
 * appropriate.
 * Parameters: client socket descriptor
 *             path to the CGI script */
/**********************************************************************/
void execute_cgi(int client, const char *path,
        const char *method, const char *query_string)
{
    char buf[1024];
    int cgi_output[2];
    int cgi_input[2];
    pid_t pid;
    int status;
    int i;
    char c;
    int numchars = 1;
    int content_length = -1;

    buf[0] = 'A'; buf[1] = '\0';
    if (strcasecmp(method, "GET") == 0)
        while ((numchars > 0) && strcmp("\n", buf))  /* read & discard headers */
            numchars = get_line(client, buf, sizeof(buf));
    else if (strcasecmp(method, "POST") == 0) /*POST*/
    {
        // 这里只是简单的对post的body长度进行记录，忽略了其他的header头
        numchars = get_line(client, buf, sizeof(buf));
        while ((numchars > 0) && strcmp("\n", buf))
        {
            buf[15] = '\0';
            if (strcasecmp(buf, "Content-Length:") == 0)
                content_length = atoi(&(buf[16]));
            numchars = get_line(client, buf, sizeof(buf));
        }
        if (content_length == -1) {
            bad_request(client);
            return;
        }
    }
    else/*HEAD or other*/
    {
    }

    // 启动了输入输出的管道，用来在两个进程之间通信
    if (pipe(cgi_output) < 0) {
        cannot_execute(client);
        return;
    }
    if (pipe(cgi_input) < 0) {
        cannot_execute(client);
        return;
    }
    // fork子进程用来返回response内容
    if ( (pid = fork()) < 0 ) {
        cannot_execute(client);
        return;
    }
    sprintf(buf, "HTTP/1.0 200 OK\r\n");
    send(client, buf, strlen(buf), 0);
    if (pid == 0)  /* child: CGI script */
    {
        char meth_env[255];
        char query_env[255];
        char length_env[255];
        // 将标准输入输出重定向到管道给父进程使用
        dup2(cgi_output[1], STDOUT);
        dup2(cgi_input[0], STDIN);
        close(cgi_output[0]);
        close(cgi_input[1]);
        sprintf(meth_env, "REQUEST_METHOD=%s", method);
        putenv(meth_env);
        // get会设置全局变量"QUERY_STRING=%s"
        if (strcasecmp(method, "GET") == 0) {
            sprintf(query_env, "QUERY_STRING=%s", query_string);
            putenv(query_env);
        }
        else {   /* POST */
            // post会设置全局变量"CONTENT_LENGTH=%d"
            sprintf(length_env, "CONTENT_LENGTH=%d", content_length);
            putenv(length_env);
        }
        execl(path, NULL);
        exit(0);
    } else {    /* parent */
        close(cgi_output[1]);
        close(cgi_input[0]);
        if (strcasecmp(method, "POST") == 0)
            for (i = 0; i < content_length; i++) {
                recv(client, &c, 1, 0);
                write(cgi_input[1], &c, 1);
            }
        while (read(cgi_output[0], &c, 1) > 0)
            send(client, &c, 1, 0);

        close(cgi_output[0]);
        close(cgi_input[1]);
        waitpid(pid, &status, 0);
    }
}
```

## 管道与子进程相关函数

### pipe 函数

```c
#include <unistd.h>

int pipe(int fd[2])
```
#### 函数功能

**{% asset_img 181615186626017.jpeg  %}**

#### 函数参数

fd[2]: 管道的两个文件描述符，之后就是可以直接操作这两个文件描述符

#### 函数返回值

成功 0 失败 -1

### fork 函数

```c
#include <unistd.h>

pid_t fork(void);
```
#### 函数功能

 一个进程，包括代码、数据和分配给进程的资源。fork（）函数通过系统调用创建一个与原来进程几乎完全相同的进程，也就是两个进程可以做完全相同的事，但如果初始参数或者传入的变量不同，两个进程也可以做不同的事。
一个进程调用fork（）函数后，系统先给新的进程分配资源，例如存储数据和代码的空间。然后把原来的进程的所有值都复制到新的新进程中，只有少数值与原来的进程的值不同。相当于克隆了一个自己。

#### 函数返回值

fork调用的一个奇妙之处就是它仅仅被调用一次，却能够返回两次，它可能有三种不同的返回值：
1. 在父进程中，fork返回新创建子进程的进程ID
2. 在子进程中，fork返回0
3. 如果出现错误，fork返回一个负值

### dup2 函数

```c
#include <unistd.h>

int dup2(int odlfd, int newfd);
```
#### 函数功能

将newfd指向oldfd所指的文件，相当于重定向功能。如果newfd已经指向一个已经打开的文件，那么他会首先关闭这个文件，然后在使newfd指向oldfd文件；
```c
int oldfd;
oldfd = open("app_log", (O_RDWR | O_CREATE), 0644);
dup2(oldfd, 1);
close(oldfd);
```

#### 函数参数

oldfd: 目标描述符
newfd: 源描述符

#### 函数返回值

成功 oldfd 失败 -1

### putenv 函数

```c
#include <stdlib.h>

int putenv(const char * string);
```
#### 函数功能

putenv()用来改变或增加环境变量的内容. 参数string 的格式为name＝value, 如果该环境变量原先存在, 则变量内容会依参数string 改变, 否则此参数内容会成为新的环境变量.
```c
#include <stdlib.h>
main()
{
   char *p;
   if((p = getenv("USER")))
   printf("USER =%s\n", p);
   putenv("USER=test");
   printf("USER=%s\n", getenv("USER"));
}
```
USER=root
USER=test

#### 函数参数

string: “name＝value”

#### 函数返回值

执行成功则返回0, 有错误发生则返回-1.

## 参考资料
1. [socket编程——sockaddr_in结构体操作](https://www.cnblogs.com/zhouhbing/p/3844484.html)
2. [pthread_create 函数](http://c.biancheng.net/view/8607.html)
3. [setsockopt函数功能及参数详解](https://www.cnblogs.com/cthon/p/9270778.html)
4. [socket编程 -- socket、bind、accept、connect函数](https://blog.csdn.net/y396397735/article/details/50655363)
5. [文件描述符详解](http://c.biancheng.net/view/2123.html)
6. [Socket send函数和recv函数详解](https://www.cnblogs.com/jianqiang2010/archive/2010/08/20/1804598.html)
7. [C语言stat()函数：获取文件状态](http://c.biancheng.net/cpp/html/326.html)
8. [fork入门知识](https://www.cnblogs.com/jeakon/archive/2012/05/26/2816828.html)
9. [fork()、pipe()、dup2() 和 execlp() 的组合技法](https://blog.csdn.net/victoryckl/article/details/17335661)
10. [pipe 函数 （C语言）](https://blog.51cto.com/huyoung/436494)
11. [C语言中dup和dup2函数的不同和使用](http://www.01happy.com/c-dup-dup2/)