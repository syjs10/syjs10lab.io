---
title: MySQL索引
date: 2017-07-11 22:45:36
tags:
  - MySQL
  - Linux 
---
> 设计数据库时遇到索引问题，故记录一下学习的笔记

<!--more-->

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;首先，介绍一下索引。当一个数据表的数据数据很多的时候，进行一次查询是十分消耗内存的，因为数据库需要把所有数据都查询一遍。
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;添加索引相当于给这个表的数据制作了一个目录，并保存成一个索引文件，查询时候直接查阅索引（目录）可以直接定位到数据所在行，大大加快查询速度。
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;然而，需要注意：并不是索引建立的越多越好的，因为建立了索引的话每次INSERT，UPDATE，DELETE的时候，系统需要更新索引文件，大大降低了表的写性能，而且索引文件也会变得很大。所以请谨慎建立索引。

下面介绍几种常见用法：

### 普通索引

基本的索引，本身没有任何限制

1.已有表添加索引

```sql
CREATE TABLE test (
	id NOT NULL,
	username VARCHAR(16) NOT NULL,
);
CREATE INDEX [indexName] ON [tableName] (columnName(length));

```
解释一下length，是取所选列的数据长度。

2.修改方式添加索引

```sql
ALTER [tableName] ADD INDEX [indexName] ON (columnName(length))；
```

3.建表时候添加索引

```sql
CREATE TABLE test (
	id NOT NULL,
	username VARCHAR(16) NOT NULL,
	INDEX [indexName] (username(length))
);

```

4.删除索引

```sql
DROP INDEX [indexName] ON [tableName];
```
### 唯一索引

它与前面的普通索引类似，不同的就是：索引列的值必须唯一，但允许有空值。

1.已有表添加索引

```sql
CREATE TABLE test (
	id NOT NULL,
	username VARCHAR(16) NOT NULL,
);
CREATE UNIQUE INDEX [indexName] ON [tableName] (columnName(length));

```


2.修改方式添加索引

```sql
ALTER [tableName] ADD UNIQUE [indexName] ON (columnName(length))；
```

3.建表时候添加索引

```sql
CREATE TABLE test (
	id NOT NULL,
	username VARCHAR(16) NOT NULL,
	UNIQUE [indexName] (username(length))
);
```
### 主键索引

它是一种特殊的唯一索引，不允许有空值。一般是在建表的时候同时创建主键索引,一个表只能有一个主键。

```sql
CREATE TABLE test (
	id NOT NULL,
	username VARCHAR(16) NOT NULL,
	PRIMARY KEY(id)
);
```

### 组合索引

为了形象地对比单列索引和组合索引，为表添加多个字段：

```sql
CREATE TABLE test (
	id NOT NULL,
	username VARCHAR(16) NOT NULL,
	city VARCHAR(50) NOT NULL,
	age INT NOT NULL
);
```
```sql
ALTER TABLE mytable ADD INDEX name_city_age (name(10),city,age);
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;建表时，usernname长度为16，这里用10。这是因为一般情况下名字的长度不会超过10，这样会加速索引查询速度，还会减少索引文件的大小，提高INSERT的更新速度。
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如果分别在 usernname，city，age上建立单列索引，让该表有3个单列索引，查询时和上述的组合索引效率也会大不一样，远远低于我们的组合索引。虽然此时有了三个索引，但MySQL只能用到其中的那个它认为似乎是最有效率的单列索引。
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;建立这样的组合索引，其实是相当于分别建立了下面三组组合索引：usernname,city,age   usernname,city   usernname  为什么没有 city，age这样的组合索引呢？这是因为MySQL组合索引“最左前缀”的结果。简单的理解就是只从最左面的开始组合。并不是只要包含这三列的查询都会用到该组合索引，下面的几个SQL就会用到这个组合索引：

```sql
SELECT * FROM mytable WHREE username="admin" AND city="郑州";  
SELECT * FROM mytable WHREE username="admin";
```

希望对大家有所帮助。