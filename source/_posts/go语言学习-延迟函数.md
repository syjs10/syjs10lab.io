---
title: go语言学习-延迟函数
date: 2020-05-17 16:30:23
tags: Go
---

>   文章介绍延迟函数（defer） 的几个常用技巧

<!-- more -->

## 什么是延迟函数

延迟函数简单的解释就是在函数return之后执行的函数

```go
func test(a int) int {
    defer func () {
        fmt.Prinfln("defer return")
    } ()
    return 0
}
```

## 延迟函数的原理

函数完成放回时候需要执行一个RET指令。。

**return最先给返回值赋值；接着 defer 开始执行一些收尾工作；最后 RET 指令携带返回值退出函数。**

1.  多个 defer 的执行顺序为“后进先出/先进后出”。所有函数在执行 RET 返回指令之前，都会先检查是否存在 defer 语句，若存在则先逆序调用 defer 语句进行收尾工作再退出返回。

    ```go
    func test() int {
        defer func() {
            fmt.Println("defer 1") // 后打印
        }()
        defer func() {
            fmt.Println("defer 2") // 先打印
        }()
        return 0 
    }
    ```

    

2.  匿名返回值是在 return 执行时被声明，有名返回值则是在函数声明的同时被声明，因此在 defer 语句中只能访问有名返回值，而不能直接访问匿名返回值。

    A. 函数的返回值没有被提前声名，其值来自于其他变量的赋值，defer 根本无法直接访问到返回值，因此函数退出时返回值并没有被修改。

    ```go
    func test() int {
        var i int
        defer func() {
            i += 1
            fmt.Println(i) // 返回1
        }()
        return i // 返回0
    }
    ```

    B. 函数的返回值被提前声名，这使得 defer 可以访问该返回值，因此在 return 赋值返回值 i 之后，defer 调用返回值 i 并进行了修改，最后致使 return 调用 RET 退出函数后的返回值才会是 defer 修改过的值。

    ```go
    func test() (i int) {
        defer func() {
            fmt.Println(i)// 返回1
            i += 1
        }()
        return i // 返回1
    }
    ```
    
3. defer声明时会先计算确定参数的值，defer推迟执行的仅是其函数体

    ```go
    package main
    
    import (
        "fmt"
        "time"
    )
    
    func main() {
        // 这两个时间不同是因为defer先计算了确定的函数参数值
        defer P(time.Now())
        // 可以通过传入函数的方式传入时间样就不会被提前
        defer P1(func () time.Time {return time.Now()})
        time.Sleep(time.Second)
    }
    
    func P(t time.Time) {
        fmt.Println("P defer", t)
        fmt.Println("P now", time.Now())
    }
    func P1(t func() (time.Time)) {
        fmt.Println("P1 defer", t())
        fmt.Println("P1 now", time.Now())
    }
    // 返回结果
    // P1 defer 2020-05-17 18:14:58 +0800 CST m=+1.004997690
    // P1 now 2020-05-17 18:14:58 +0800 CST m=+1.005635828
    // P defer 2020-05-17 18:14:57 +0800 CST m=+0.000081260
    // P now 2020-05-17 18:14:58 +0800 CST m=+1.005661020
    ```

## defer常用的场景

1.  用于某些需要进行对称的操作的地方，比如打开文件之后关闭文件，或者加锁后解锁，这些操作都可以用defer实现。

    ```go
    func ReadFile(filename string) ([]byte, error) {
        f, err := os.Open(filename)
        if err != nil {
            return nil, err
        }
        defer f.Close()//这里关闭文件句柄
        return ReadAll(f)
    }
    ```

    

2.  调试时候或者打印函数执行时间

    ```go
    func BigSlowOperation() {
        defer trace("BigSlowOperation") ()
    	// 代码段
        time.Sleep(10 * time.Second)
    }
    //这样用到了第三条特性， defer声明时会先计算确定参数的值，defer推迟执行的仅是其函数体
    func trace(msg string) func() {
        start := time.Now()
        log.Printf("enter %s", msg)
        return func() {
            log.Printf("exit %s (%s)", msg, time.Since(start))
        }
    }
    ```

    

3.  宕机时候用来捕获异常，通过判断传入条件，选择是否宕机，或者用来处理宕机情况防止整个程序挂掉

    ```go
    func soleTitle(doc *html.Node) (title string, err error) {
    	type bailout struct{}
    	defer func() {
    		switch p := recover(); p {
    		case nil: // no panic
    		case bailout{}: // "expected" panic
    			err = fmt.Errorf("multiple title elements")
    		default:
    			panic(p) // unexpected panic; carry on panicking
    		}
    	}()
    	// Bail out of recursion if we find more than one nonempty title.
    	forEachNode(doc, func(n *html.Node) {
    		if n.Type == html.ElementNode && n.Data == "title" && n.FirstChild != nil {
    			if title != "" {
    				panic(bailout{}) // multiple titleelements
    			}
    			title = n.FirstChild.Data
    		}
    	}, nil)
    	if title == "" {
    		return "", fmt.Errorf("no title element")
    	}
    	return title, nil
    }
    ```

## 参考文献

-   https://studygolang.com/articles/11522
-   《Go程序设计语言》