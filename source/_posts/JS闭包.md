title: javascript闭包问题
date: 2016-10-15 22:10:23
tags:
  - javascript
---

> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;------本文转载自liufang169的博客

<!--more-->
## 先来个简单的函数

```javascript
var liufang169 = function(){
    var name = "liufang";
    return name;
}

```


额，上面的函数执行后，就会返回name的值。。。额，这有啥东西么。。。。。。。。。。。。
其实，这些就是一切的起点。。。


# 闭包是函数

改一改上面的代码。。。

```javascript
var liufang169 = function(){
  var name = "liufang"
  return function (){
    return name;
  }
}

var liufang = liufang169();


liufang();

```

以上代码也是返回name的值。。。。。。那么，问什么非要写这么复杂呢？？？

首先先声明一点，闭包是函数！！！ 注意提取主谓宾啊，就是闭包是函数！！！

解释一下上面的代码，我们先定义了一个函数，叫liufang169 （好吧我是函数）,那么这个函数里面有一个内部的变量，然后返回值是一个函数。

那么，外面定义的liufang这个变量，他是一个函数，这个函数是liufang169返回的那个（此处留flag，注意）。然后在执行liufang ，输出name的值

so，，，下面要说结论了。liufang就是liufang169闭包！！！！！！！因为它获取了liufang169里面定义的变量！

那么，，，，这到底有什么用（doge 脸。。。。。。。。


# 闭包有毛用 && 进一步理解闭包

那么，现在我们看看另一段函数


```javascript

var syjs10 = function () {
  var a = 0;
  var b = 0;

  return function () {
    return a += 1;
  }

}


var yuangezhizao = syjs10();

yuangezhizao() //返回1

yuangezhizao() //返回2

yuangezhizao() //返回3

yuangezhizao()  //返回4


```

以上代码中： 函数yuangezhizao 是 syjs10的闭包 ，因为yuangezhizao 获取了 syjs10 内部变量

那么，，，重点来了，，， 每次执行yuangezhizao时，a的值是不被回收的，也就是a的值被保留了，每次的值都累计了。

为什么呢？？？

首先，什么是执行了函数？？就是 函数名() 酱紫。那么yuangezhizao里面是什么，其实是函数，（如上）没执行的函数。你可以innerHTML看一下，

那么 这个函数在这段程序里, 你var的 yuangezhizao 是一直存在的，对吧，那么也就是说，yuangezhizao 是syjs10返回的那个函数的一个实例，

就是说 yuangezhizao = function(){ return a+=1 }，那也就是function (){ return a += 1}一直存在，（并不是syjs10里面的那个，而

是yuangezhizao）那么，a就得一直存在，所以在程序结束前，这个a一直存在！！！  不被回收！
