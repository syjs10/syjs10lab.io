---
title: Smarty使用模板
date: 2017-01-30 18:34:28
tags:
---
> 今天研究了一天如何使用模板，然而网上的方法都不好使

<!-- more -->
```PHP
	<{block name=head}>
		<title>test</title>
	<{/block}>
	<{block name=body}>
		<div>Hello World!</div>
	<{/block}>
```
这样可以分别在模板head里和body里添加内容