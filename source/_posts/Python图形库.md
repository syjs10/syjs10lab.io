---
title: Python图形库
date: 2016-10-14 15:32:27
tags:
 - python
 - 验证码识别
 - 爬虫
---
> 研究了好久终于在今天研究出了如何优雅简洁的安装上python图形库,下面我就里介绍一下搭建方法


<!--more-->
# python图形库安装教程
##安装pip
Install pip and virtualenv for Ubuntu 10.10 Maverick and newer

      sudo apt-get install python-pip python-dev build-essential
      sudo pip install --upgrade pip
      sudo pip install --upgrade virtualenv

Install Easy Install

      sudo apt-get install python-setuptools python-dev build-essential

Install pip

      sudo easy_install pip

Install virtualenv

      sudo pip install --upgrade virtualenv

## ubuntu版
1.tesseract-ocr安装

      sudo apt-get install tesseract-ocr

2.pytesseract安装

      sudo pip install pytesseract

3.Pillow 安装

      sudo pip install pillow

## 其他Linux版本（如centos）

1.tesseract-ocr安装
没找到直接命令安装，所以需要手动下载安装包。
https://github.com/tesseract-ocr/tesseract
在上述地址中下载最新的tesseract-ocr的安装包，并解压。
通过以下命令安装：

      cd tesseract-3.04.01
      ./autogen.sh
      ./configure

注意，如果出现error: leptonica not found，需要下载安装leptonica
http://www.leptonica.org/download.html

      make

      make install

      ldconfig

2.pytesseract安装

      sudo pip install pytesseract

3.Pillow 安装

      sudo pip install pillow
